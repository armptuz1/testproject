package com.plusitsolution.traning.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CalculatorController {
	
	private List<String> histories = new ArrayList<String>();

	@PostMapping("/plus")
	public double plus(@RequestParam("number1") double number1, @RequestParam("number2") double number2) {
		double result = number1 + number2;
		histories.add(Double.toString(number1) + " + " + Double.toString(number2) + " = " + Double.toString(result));
		return result;
	}
	
	@PostMapping("/minus")
	public double minus(@RequestParam("number1") double number1, @RequestParam("number2") double number2) {
		double result = number1 - number2;
		histories.add(Double.toString(number1) + " - " + Double.toString(number2) + " = " + Double.toString(result));
		return result;
	}
	
	@PostMapping("/multiple")
	public double multiple(@RequestParam("number1") double number1, @RequestParam("number2") double number2) {
		double result = number1 * number2;
		histories.add(Double.toString(number1) + " * " + Double.toString(number2) + " = " + Double.toString(result));
		return result;
	}
	
	@PostMapping("/divide")
	public double divide(@RequestParam("number1") double number1, @RequestParam("number2") double number2) {
		double result = number1 / number2;
		histories.add(Double.toString(number1) + " / " + Double.toString(number2) + " = " + Double.toString(result));
		return result;
	}

	@GetMapping("/histories")
	public List<String> histories() {
		return histories;
	}

}
